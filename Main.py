import problem1
import problem2
import FixedPointIter
import problem4
import problem5
import problem6


if __name__ == "__main__":
    do_exer = True
    while  do_exer:
        exercise = int(input("Type Exercise number : "))
        if  exercise == 0:
            do_exer = False
        elif exercise == 1:
            print("Problem 1.a ")
            I =problem1.numpymatrix(3)
            print(I)
            print("----------------------------------------------------------------------------------------------------")
            print("Problem 1.b")
            matrix = problem1.matman(2,0,3,I)
            matrix = problem1.matman(0,2,3,matrix)
            print(matrix)
            print("----------------------------------------------------------------------------------------------------")
        elif exercise == 2:
            print("Problem 2")
            nice = problem2.numpylinespace(-2,3,11)
            nice = problem2.replacezero([2,3,4],nice)
            print(nice)
            print("----------------------------------------------------------------------------------------------------")
        elif exercise == 3:
            print("Problem 3")
            print("Nice plotting interval is -1.6,1.6")
            xf = FPI(f1,1.5,1.0e-4,20)
            print("Nice plotting interval is -1.6,1.6")
            xp = FPI(f2,1.5,1.0e-4,20)
            print("The f1, function takes the cosinus of x, and then subtract x. So for x < 0, f will be quire large, and for x > 0 it will be quite small.")
            print("In other words, the fixed point iter, moves \" slowly \" towords the fixed point. It is expected that within 20 iteration it will not converge to the fixed point")
            print("The f2, on the other hand is the cosinus, with the half amplitude. So this will act as a normal cosinus, and therefor it is expected to converge to the fixed point.")
            print("----------------------------------------------------------------------------------------------------")
        elif exercise == 4:
            print("Poblem 4")
            problem4.Plotpoly([1,0,-7,6])
        elif exercise == 5:
            xx = np.linspace(.1,2,200)
            problem5.make_figure(xx,lambda xx: np.exp(xx), "Exsponentiali")



        else:
            do_exer = False
