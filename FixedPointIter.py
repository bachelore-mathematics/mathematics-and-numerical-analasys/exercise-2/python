#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Introductory examples: Fixed point iteration

@author: Ralf Zimmermann
"""

import numpy as np
import matplotlib.pyplot as plt


#------------------------------------------------------------------------------
# define some test functions
# f01, f02, f03 correspond to examples E. 01, E. 02, E. 03
# from the lecture notes
#------------------------------------------------------------------------------
def f01(x):
    return (1.0/5.0)*(x**3 + x**2-3)
    # a proper plot interval for f01 is [a,b] = [-1,1]
    # a suitable starting point is x0=0
    
def f02(x):
    return (1.0/7.0)*(x**3 + 2*x**2 - 1)
    # a proper plot interval for f02 is [a,b] = [1,3]
    # a suitable starting point is x0=2

def f03(x):
    return np.floor(x) + 1 - 0.5*(np.floor(x) + 1 - x)**2
    # a proper plot interval for f03 is [a,b] = [1,4]
    # a suitable starting point is x0=1.5
    # this function does not have a fixed point
#------------------------------------------------------------------------------

# additional examples
def f1(x):
    return np.cos(x)-x
    # a proper plot interval for f1 is [a,b] = [-1.6,1.6]
    # a suitable starting point is x0=0
    # FPI for this function does not converge

def f2(x):
    return 0.5*np.cos(x)
    # a proper plot interval for f2 is [a,b] = [0,1.6]
    # a suitable starting point is x0=0


def f3(x):
    return 2.0/x**2 -1 +x
    # a proper plot interval for f3 is [a,b] = [1,3]
    # a suitable starting point is x0=2

#------------------------------------------------------------------------------
# The function FPI performs fixed point iteration for a real valued function
# The course of the iterations is also plotted;
# this is for educational purposes only.
#
#
# Inputs:
#     f   : function
#     x0  : starting point
#    tol  : numerical tolerance for detecting convergence
# MaxIter : max. number of iterations (avoid infinite loops)
#
# Outputs:
#     x   : fixed point, if the method converged, 
#          otherwise final iteration
#------------------------------------------------------------------------------    
def FPI(f,x0,tol,MaxIter):
    #
    # FPI: Fixed Point Iteration
    #  
    
    # let the user choose the interval bounds
    a = input("Lower interval bound: ")
    # make input a floating point number
    a = float(a)
    b = input("Lower interval bound: ")
    # make input a floating point number
    b = float(b)
    
    # resolution for the plot
    res = 1001
    #evenly spaced points in [a,b]
    int_ab = np.linspace(a,b,res)
    
    # compute function values on [a,b]
    f_ab = f(int_ab)
    
    # create plot object
    fig, ax = plt.subplots()
    
    # plot the function and the identity line:
    # we are looking for a point, where these meet
    # pattern: 
    #  ax.plot(array1, array2, options)
    #  plots the entries of array1 versus the entries of array2
    #  the option may define the color and type of the plot
    #  Example: 'k-' is for a black line, 'k--' is for a dashed black line
    #           'b:' is for a dotted blue line,...
    # The linewidth may also be specified.
    
    #a) plot the function in solid black
    ax.plot(int_ab, f_ab, 'k-')
    #b) plot the identity line in dashed red
    ax.plot(int_ab, int_ab, 'r--')

    # iteration counter
    iter=0
    #initial value
    x=x0
    #stopping flag
    notyetdone=True
    
    while (iter<MaxIter) and notyetdone:
        # this is the actual FPI
        xold=x
        x=f(x)
        # here, the actual FPI is already done, 
        # rest is plotting and checking for convergence

        #plot old point
        old_pt,   = ax.plot(np.array([xold]), np.array([xold]),  'bo', linewidth=3)
        # plot line from (xold, xold) on identity line to new (xold, x)
        # recall: x = f(xold), so that (xold, x) is on the graph of f
        new_pts,  = ax.plot(np.array([xold,xold]), np.array([xold,x]),  'b-', linewidth=3)
        # plot line from (xold, x)  to new (x, x) on identity line
        new_pts2, = ax.plot(np.array([xold,x]), np.array([x,x]),  'b-', linewidth=3)
        
        # check for convergence: 
        # the result of a term "(x>y)" is a boolean value: "True" or "False"
        notyetdone = (abs(x-xold)/max(abs(x),1))>tol
        # update iteration counter
        iter=iter+1
        # print some onscreen infos
        print('Current x =', x, ' at iteration ', iter)
        # wait for keyboard input to continue
        input("Press key to continue")
    # end while....
    
    #make the plots
    plt.show()
    # return the fixed(?) point
    return x
# End: FPI


# perform test
#xf = FPI(f1,0.2,1.0e-4,20)

