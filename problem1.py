import numpy as np

# numpymatrix 
#
# Description
# Create a numpy matrix of any size
#
# Input : size, int size of the matrix, e.g. 2 for a 2 by 2
# Output : mat, int, the matrix formed by the system 
# Catch : none
def numpymatrix(size):
    mat = np.eye(size)
    return mat

# matman 
#
# Description
# Manipulate matrix
#
# Input : row, int, the row number to manipulate
#         col, int, the coloumn number to manipulate
#         val, the value to insert
#         mat, the matrix to manipulate
# Output : mat, float, the new matrix
# Catch : none
def matman(row,col,val,mat):
    mat[row][col] = val
    return mat

