import sympy as sym


# LagrandeInterpolation 
#
# Description
# Perform the Lagrande interpolation. 
#
# Input : xi, list, A list of the x value from the dataset
#         fi, list, A list of the f(x) value from the dataset
# Output : pn, symbol, The polynomial of n degree. 
# Catch : none
def LagrandeInterpolation(xi,fi):
    # Dummy check
    if len(xi) != len(fi):
        print("Dumbass xi and fi is not the same length")
        return False
    # Initialize variables. 
    pn = 0 # pn, the polynomial of degree n. 
    x = sym.Symbol('x') # Symbolic x value. 
    for i in range(len(xi)): # The algorithm is created from theroem 1. 
        # i for loop, is summarizing the different parts of the equations. 
        L = 1 # Variable with the product loop, reset to 1 each iteration so the product starts from the beginning.
        for j in range(len(xi)): # Product loop goes from j = 0 to n = 3
            if  i == j: # Skip the i = j scenario. 
                continue
            L = L * ((x - xi[j]) / (xi[i]-xi[j]) ) # Perform the product, take the previuos L and multiply with the next
        pn = pn + fi[i] * L # Perform the sum, with the newly calculated product. 
    pn = sym.simplify(pn) # Simplify the expansion, easy for human brains to concieve. 
    return pn # Return the polynomial. 

# Main function, not much to say. 
if __name__ == "__main__":
    # Problem 2.a
    xi = [0,1,2,3] # Dataset from the exercise sheet 
    fi = [1,4,11,22] # Data set from the exercise sheet
    print(LagrandeInterpolation(xi,fi)) # Use Lagrande Interpolation to calculate the polynomial
    # Problem 2.b 
    xi = [0,1,3] # Dataset from the exercise sheet
    fi = [1,4,22] # Dataset from the exercise sheet
    print(LagrandeInterpolation(xi,fi)) # Use the Lagrande Interpolation to calculate the polynomial
