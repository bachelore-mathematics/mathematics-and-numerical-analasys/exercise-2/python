gmport numpy as np

# numpylinespace 
#
# Description
# Create a list of items
#
# Input : lb, int, lower boundary
#         ub, int, upper boundary
#         n, int, number of elements
# Output : list_rtn, floats, the created list
# Catch : none
def numpylinespace(lb,ub,n):
    list_rtn = np.linspace(lb,ub,n)
    return list_rtn

# replacezero 
#
# Description
# reaplace some elements with zero
#
# Input : ele, list, list of elements to replace with zero. 
#         list_rtn, list, input list. 
# Output : list_rtn, float, the resulting list
# Catch : none
def replacezero(ele,list_rtn):
    for i in ele:
        list_rtn[i] = 0
    return list_rtn
