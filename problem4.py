import numpy as np
import matplotlib.pyplot as plt

# Plotpoly 
#
# Description
# Plot a polynomial using only a list as input
#
# Input : deg, list, list of the coefficient
# Output : plot, plot of the polynomial
# Catch : none
def Plotpoly(deg):
    pol = np.poly1d(deg)
    print("From the following values : ")
    print(deg) 
    print(" the following polynomial can be concluded :")
    print(pol)
    x = np.linspace(-3.5,3.5,500)
    plt.plot(x,pol(x), '-')
    plt.axhline(y=0)
    plt.title('A polynomial of order 3')
    plt.show()
