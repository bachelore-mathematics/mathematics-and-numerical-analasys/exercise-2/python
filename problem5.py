import numpy as np
import matplotlib.pyplot as plt

# make_figure 
#
# Description
# Create and save a figure
#
# Input : x, the x value
#         y, function callback for y values
#         name, the name of the plot
# Output : plot
# Catch : none
def make_figure(x,f,name):
    plt.figure(figsize=(9,2))
    ax1 = plt.subplot(141)
    ax1.plot(x, f(x), 'k', lw=2)
    plt.title(name)

    ax2 = plt.subplot(142)
    ax2.semilogx(x,f(x), 'k', lw=2)
    ax2.set_title("Semi-Logarithmic x")

    ax3 = plt.subplot(143)
    ax3.semilogy(x,f(x), 'k', lw=2)
    ax3.set_title("semi-logarithmic y")

    ax4 = plt.subplot(144)
    ax4.loglog(x, f(x), 'k', lw=2)
    ax4.set_title("Logarithimic")
    plt.savefig(name+".png")
    plt.show()
